const express=require('express')

const router=express.Router();
const cryptoJS=require('crypto-js')
const mysql=require('mysql2');
const db=require('../db')
const utils=require('../utils');
const { request } = require('http');
const { response } = require('express');

router.get('/',(request,response)=>{
    // console.log("hi")
const query='select mid,mtitle,date,time,dname from movie';
db.pool.execute(query,(error,movie)=>{
    response.send(utils.createResult(error,movie))
})

router.post('/',(request,response)=>{
    const {mtitle,date,time,dname}=request.body;
    const query='insert into movie (mtitle,date,time,dname) values(?,?,?,?)';

    db.pool.execute(query,[mtitle,date,time,dname],(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

// router.get('/get',(request,response)=>{
//     const {id}=request.body;
//     const query='select firstName,email,password from person where id=?';
//     db.pool.execute(query,[id],(error,person)=>{
//         if(person.length >0)
//         {
//             response.send(utils.createResult(error,person));
//         }
//         else{
//             response.send("Person not found!!");
//         }
        
//     })
// })

router.delete('/',(request,response)=>{
    console.log("here")
    const {mid}=request.body;
    const query='delete from movie where mid=?'
    db.pool.execute(query,[mid],(error,movie)=>{
        response.send(utils.createResult(error,movie));
    })
})

})
module.exports= router
